# Overview

LibReDE is a library for resource demand estimation. Resource demands are a common input parameter to stochastic performance models (e.g., Queueing Networks, or Queueing Petri Nets). LibReDE helps to determine resource demand values based on monitoring data from a system (e.g., CPU utilization, response time, or throughput).

# Downloads

Binary downloads are coming soon...

# Documentation

A user documentation is currently prepared...