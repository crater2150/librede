package edu.kit.ipd.descartes.librede.estimation.validation;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.kit.ipd.descartes.librede.estimation.validation.partitioning.IterativePartitioner;
import edu.kit.ipd.descartes.librede.estimation.validation.partitioning.SingleRunPartitioner;

public class PartitionerTest {
	
	private final int KFOLD = 5;
	private final int INTERVALS = 50;
	private final int EXPECTED_SIZE = INTERVALS / KFOLD;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testSingleRunPartitioner() {
		SingleRunPartitioner part = new SingleRunPartitioner(KFOLD, INTERVALS);
		
		int[] partitionSizes = new int[KFOLD];
		for(int i = 0; i < INTERVALS; i++) {
			partitionSizes[part.getPartition(i)]++;
		}
		
		for(int size : partitionSizes) {
			assertThat(Math.abs(size - EXPECTED_SIZE)).isLessThanOrEqualTo(1);
		}
	}
	
	@Test
	public void testIterativePartitioner() {
		IterativePartitioner part = new IterativePartitioner(KFOLD, INTERVALS);
		
		int[] partitionSizes = new int[KFOLD];
		for(int i = 0; i < INTERVALS; i++) {
			partitionSizes[part.getPartition(i)]++;
		}
		
		for(int size : partitionSizes) {
			assertThat(Math.abs(size - EXPECTED_SIZE)).isLessThanOrEqualTo(EXPECTED_SIZE);
		}
	}
}
