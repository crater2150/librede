package edu.kit.ipd.descartes.librede.estimation.validation.partitioning;

/**
 * Create partitions for cross validation.
 */
public interface Partitioner {
	/**
	 * Get the partition number for an element
	 * @param index index of an element
	 * @return partition number for that element
	 */
	public int getPartition(int index);
}
