package edu.kit.ipd.descartes.librede.estimation.validation.partitioning;

import java.util.Arrays;

import cern.jet.random.sampling.RandomSampler;

public class SingleRunPartitioner implements Partitioner {
	private int[] partitions;
	
	public SingleRunPartitioner(int kfold, int intervals){
		partitions = new int[intervals];
		int partitionSize = (int)Math.ceil(intervals / (double)kfold);
		Arrays.fill(partitions, -1);
		
		for (int i = 0; i < kfold - 1; i++) {
			int[] subset = new int[intervals - partitionSize * i];
			for (int p = 0, s = 0; p < intervals; p++) {
				if (partitions[p] < 0) {
					subset[s] = p;
					s++;
				}
			}
			long[] samples = new long[partitionSize];
			RandomSampler.sample(partitionSize, subset.length, partitionSize, 0, samples, 0, null);
			for (int s = 0; s < samples.length; s++) {
				partitions[subset[(int)samples[s]]] = i;
			}
		}
		
		for (int i = 0; i < intervals; i++) {
			if (partitions[i] < 0) {
				partitions[i] = kfold - 1;
			}
		}
	}
	
	@Override
	public int getPartition(int index) {
		return partitions[index];
	}
}
