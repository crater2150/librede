package edu.kit.ipd.descartes.librede.estimation.validation.partitioning;

import java.util.Arrays;
import cern.jet.random.*;

public class IterativePartitioner implements Partitioner {

	private int kfold;
	private int[] partitions;
	private int[] partitionSize;
	private int lastIndex = -1;
	private double[] partitionProbs;
	double reciprocal_sum;
	double[] reciprocals;

	public IterativePartitioner(int kfold, int intervals) {
		this.kfold = kfold;

		this.partitions = new int[intervals];
		this.partitionSize = new int[kfold];
		this.partitionProbs = new double[kfold];
		this.reciprocal_sum = kfold;
		this.reciprocals = new double[kfold];

		Arrays.fill(partitions, -1);
		Arrays.fill(partitionSize, 0);
		Arrays.fill(partitionProbs, 1 / (double) kfold);
		Arrays.fill(reciprocals, 1);
	}

	@Override
	public int getPartition(int index) {
		if (lastIndex < index) {
			for (int i = lastIndex + 1; i <= index; i++) {
				updatePartitions(i);
			}
			lastIndex = index;
		}
		return partitions[index];
	}

	private void updatePartitions(int index) {
		double rand = Uniform.staticNextDoubleFromTo(0.0, 1.0);
		int partition = 0;
		double probability = 0;
		for (; partition < kfold; partition++) {
			probability += partitionProbs[partition];
			if (probability >= rand) {
				break;
			}
		}

		if (partitions[index] != -1) {
			partitionSize[partitions[index]]--;
			updateProbabilities(partitions[index]);
		}

		partitions[index] = partition;
		partitionSize[partition]++;
		updateProbabilities(partition);
	}

	private void updateProbabilities(int affected) {
		double old_reciprocal = reciprocals[affected];
		reciprocals[affected] = 1.0 / (partitionSize[affected] + 1);
		reciprocal_sum += reciprocals[affected] - old_reciprocal;

		for (int p = 0; p < kfold; p++) {
			partitionProbs[p] = reciprocals[p] / reciprocal_sum;
		}
	}
	
	public static void main(String[] args) {
		
	}

}
